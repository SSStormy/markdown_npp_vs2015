# Markdown Syntax highlighting in VS2015 flavor for Notepad++
Markdown syntax formatting, originally forked from [here](sourceRepo), but in a dark [VS2015 flavor](https://github.com/Nidre/VS2015-Dark-Npp).

**Note:** this doesn't support Zenburn, unlike Edditoria's version.

## Installation

1. Naviagte to `%APPDATA%/Notepad++`
2. Open `userDefineLang.xml` (create one if it doesn't exist) and copy over the contents of the one in this repo to it.
3. Restart Notepad++
4. Naviagte to Language and chose `Markdown-VS2015` at the bottom.

## Copyright Notice
###### Originally taken from [this repo.](sourceRepo)

*You may take it, use it, modify it, but you should not sell it.*

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

[sourceRepo]: https://github.com/Edditoria/markdown_npp_zenburn
